/*
 * Doubly-linked list supporting insertion into the bottom and removal at either end.
 */

typedef struct Node
{
	char data;
	struct Node* next;
	struct Node* prev;
} Node;

typedef struct LinkedList
{
	struct Node* bottom;
	struct Node* top;
	int size;
} LinkedList;

LinkedList* getList();
void push(LinkedList*, char);
char pop( LinkedList*);
char peek(LinkedList*);
char dequeue(LinkedList*);
void clear(LinkedList*);
int size(LinkedList*);
