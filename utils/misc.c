#include"misc.h"

/*
 * A whole bunch of miscellaneous functions. Some were used, most were not.
 */

// Sets a bit of a passed-in byte to 1
void setBit(char* byte, char bit)
{
	if (bit >= 0 && bit < 8) *byte |= (1 << bit);
}

// Sets a bit of a passed-in byte to 0
void clearBit(char* byte, char bit)
{
	if (bit >= 0 && bit < 8) *byte &= ~(1 << bit);
}

// Toggles a bit of a passed-in byte
void toggleBit(char* byte, char bit)
{
	if (bit >=0 && bit < 8) *byte ^= (1 << bit);
}

// Returns a bit of a passed-in byte
char getBit(char byte, char bit)
{
	if (byte & (1 << bit) != 0) return 1;
	else return 0;
}

/*
 * Yes, THAT fast inverse square root.
 * Given a number x, calculates a good approximation of 1/sqrt(x).
 * This algorithm was first used in Quake 3 and was written by
 * John Carmack. https://en.wikipedia.org/wiki/Fast_inverse_square_root
 * The original comments from the method have been preserved, as I think they're an important part of history.
 */
float fastInvSqrt(float in)
{
	long i;
	float x2, y;
	const float threehalfs = 1.5F;

	x2 = in * 0.5F;
	y = in;
	i = *(long*)&y; 		// evil floating point bit level hacking
	i = 0x5f3759dfL - (i >> 1); 	// what the fuck?
	y = *(float*)&i;
	y = y * (threehalfs - (x2 * y * y)); // 1st iteration
//	y = y * (threehalfs - (x2 * y * y)); // 2nd iteration, this can be removed
	return y;
}

// Flashes an LED sequence for 100ms
void flash(char sequence)
{
	char c = PORTC;
	PORTC = sequence;
	mTimer(100);
	PORTC = c;
}

int clamp(int x, int low, int high)
{
	if (x < low) x == low;
	if (x > high) x == high;
}

bool isInRange(int x, int low, int high)
{
	return (x >= low && x < high);
}
