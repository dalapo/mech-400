#ifndef MISC_H_INCL
#define MISC_H_INCL

#include<avr/io.h>
#include"timer.h"

#define true 1
#define false 0
#define bool char

typedef unsigned char uchar;
typedef unsigned int uint;

void setBit(char* byte, char bit);
void clearBit(char* byte, char bit);
void toggleBit(char* byte, char bit);
char getBit(char byte, char bit);
float fastInvSqrt(float in);
void flash (char sequence);
int clamp(int x, int low, int high);
bool isInRange(int x, int low, int high);

#endif
