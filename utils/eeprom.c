#include<avr/io.h>
#include<stdlib.h>
#include<string.h>
#include"eeprom.h"

// File to handle interfacing with flash memory (R/W)

void flashWriteChar(uint addr, char data)
{
	while (EECR & (1 << EEPE)) {} // Wait for completion of previous write
	EEAR = addr;
	EEDR = data;
	EECR |= (1 << EEMPE);
	EECR |= (1 << EEPE);
}

// Big-endian int write
void flashWriteInt(uint addr, int16_t data)
{
	char lowByte = (data & 0xFF);
	char highByte = (data >> 8) & 0xFF;
	flashWriteChar(addr, highByte);
	flashWriteChar(addr + 1, lowByte);
}

void flashWriteString(uint addr, const char* str)
{
	int len = strlen(str);
	flashWriteInt(addr, len);
	char* cpy = str;
	for (int i=0; i<len; i++)
	{
		flashWriteChar(addr + i + 2, *(cpy + i));
	}
	flashWriteChar(addr + len + 2, 0x00); // Add a null to the end
}

char flashReadChar(uint addr)
{
	while (EECR & (1 << EEPE)) {}
	EEAR = addr;
	EECR |= (1 << EERE);
	return EEDR;
}

int flashReadInt(uint addr)
{
	char highByte = flashReadChar(addr);
	char lowByte = flashReadChar(addr + 1);
	return (highByte << 8) | lowByte;
}

#warning Ensure that the returned string is passed to free() if flashReadString() is used
char* flashReadString(uint addr)
{
	int len = flashReadInt(addr);
	char* str = malloc(sizeof(char) * (len+1)); // Warning - potential memory leak
	for (int i=0; i<2; i++)
	{
		*(str + i) = flashReadChar(addr + i + 2);
	}
	*(str + len) = 0x00;
	return str;
}
