#include<avr/io.h>
#include"pwm.h"
#include"misc.h"
#include"myutils.h"

int getSize(int tmr)
{
	if (tmr == 0 || tmr == 2) return 1;
	else if (tmr == 1 || tmr == 3 || tmr == 4 || tmr == 5) return 2;
	else return -1;
}

volatile char* const TCCR_REGISTERS[] = {
	&TCCR0A, &TCCR0B, &DDRB, &DDRG,
	&TCCR1A, &TCCR1B, &DDRB, &DDRB,
	&TCCR2A, &TCCR2B, &DDRB, &DDRH,
	&TCCR3A, &TCCR3B, &DDRE, &DDRE,
	&TCCR4A, &TCCR4B, &DDRH, &DDRH,
	&TCCR5A, &TCCR5B, &DDRL, &DDRL
};
	
volatile char* const OCR_REGISTERS[] = {
	&OCR0A, &OCR0B, &OCR0B,
	&OCR1A, &OCR1B, &OCR1C,
	&OCR2A, &OCR2B, &OCR2B,
	&OCR3A, &OCR3B, &OCR3C,
	&OCR4A, &OCR4B, &OCR4C,
	&OCR5A, &OCR5B, &OCR5C
};


/*
 * The file for PWM functions.
 */

/*
 * Initializes the PWM to non-invert mode, running at 488 Hz (assuming a clock speed of 8MHz)
 * Be sure to still initialize the data directions according to need.
 * If an invalid timer ID is passed in, this function does nothing.
 */
void initPWM(int timer)
{
	if (getSize(timer) == 1) // 8-bit timer
	{
		*TCCR_REGISTERS[4*timer] |= 0xA0; // Non-invert mode
		*TCCR_REGISTERS[4*timer+1] |= 0x03; // 488 Hz
	}
	else if (getSize(timer) == 2) // 16-bit timer
	{
		*TCCR_REGISTERS[4*timer] &= 0x03; // Clear all but low 3 bits of A reg(wave gen mode)
		*TCCR_REGISTERS[4*timer] |= 0xA8; // Non-invert mode for all
		*TCCR_REGISTERS[4*timer+1] &= ~0x07; // Clear low 3 bits of B reg
		*TCCR_REGISTERS[4*timer+1] |= 0x03; // Set 1/64 prescaling (488 Hz)
	}
	/*
	TCCR0A |= 0xA0;	// 0b10100000, non-invert mode
	TCCR0B |= 0x03; // CS01 | CS00 for 488 Hz
	DDRB = 0xFF;
	DDRG = 0xFF;
	*/
}

/*
 * WGM2:0 are split up into 2 registers for some reason, and the bits aren't organized nicely, so this function
 * simplifies things. Sets the wave gen mode to one of 7 options (see pwm.h)
 */
void setWaveGen(int timer, char wave)
{
	if (getSize(timer) == 1)
	{
		// clear WGM2:0
		*TCCR_REGISTERS[4*timer] &= 0b11111100;
		*TCCR_REGISTERS[4*timer+1] &= 0b11110111;
	
		// set WGM1:0 in TCCR0A and WGM2 in TCCR0B according to the passed value
		// see pwm.h for defined modes
		*TCCR_REGISTERS[4*timer] |= (wave & 0x03);
		*TCCR_REGISTERS[4*timer+1] |= (wave & 0x04) << 1;
	}
	else if (getSize(timer) == 2)
	{
		*TCCR_REGISTERS[4*timer] &= ~0x03;
		*TCCR_REGISTERS[4*timer+1] &= ~0x18;

		*TCCR_REGISTERS[4*timer] |= (wave & 0x03);
		*TCCR_REGISTERS[4*timer+1] |= (wave & 0x0C) << 1;
	}
}


/*
// As setWaveGen but takes a 16-bit wave gen option instead
// TCCRnA: Bit 1 is WGMn1, Bit 0 is WGMn0
// TCCRnB: Bit 4 is WGMn3, Bit 3 is WGMn2
void setWaveGen3(char wave)
{
	TCCR3A &= ~0x03;
	TCCR3B &= ~0x18;

	TCCR3A |= (wave & 0x03);
	TCCR3B |= (wave & 0x0C) << 1;
}

void setWaveGen5(char wave)
{
	TCCR5A &= ~0x03;
	TCCR5B &= ~0x18;
	
	TCCR5A |= (wave & 0x03);
	TCCR5B |= (wave & 0x0C) << 1;
}
*/

// Returns the wave gen in the same format as it's set in the function above
char getWaveGen()
{
	return (TCCR0A & 0x03) | ((TCCR0B & 0x08) >> 1);
}

// Sets the duty cycle of the PWM. 0x00 is completely low, 0xFF is completely high
void setDutyCycle(int timer, int sub, uint val)
{
	if (getSize(timer) == 1)
	{
		*OCR_REGISTERS[3*timer+sub] = (char)(val & 0xFF);
	}

	else if (getSize(timer) == 2)
	{
		*OCR_REGISTERS[3*timer + sub] = val;
	}
}

uint getDutyCycle(int timer, int sub)
{
	return *OCR_REGISTERS[3*timer+sub];
}
