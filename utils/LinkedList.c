#include <stdlib.h> // for malloc and free
#include "LinkedList.h"

/*
 * Our own implementation of the FIFO queue, but FIEO (First In Either Out). Insertion is supported only on the head,
 * but removal is supported on both the head and the tail.
 *
 * Additionally, this implementation contains the whole list as its own data structure, which then contains a head and a tail.
 * This was done to reduce the number of separate variables to keep track of in the main program, and to avoid the possibility 
 * of calling insert(&head, &tail) with a head and a tail that belong to different lists.
 *
 * Finally, head and tail are referred to here as bottom and top. This is the convention we decided on.
 */

/*
 * Creates and returns a new, empty list.
 */
LinkedList* getList()
{
	LinkedList* list = malloc(sizeof(LinkedList));
	list->bottom = NULL;
	list->top = NULL;
	list->size = 0;
	return list;
}

/*
 * Creates a new node with the passed data and adds it to the bottom of the passed list.
 */
void push(LinkedList* l, char data)
{
	// Create and initialize a new node to push onto the list
	Node* node = malloc(sizeof(Node));
	node->data = data;
	node->next = NULL;
	node->prev = NULL;

	// If the list is empty, set the top of the list to the new node.
	// Otherwise, link the bottom of the list to the new node.
	if (l->size == 0)
	{
		l->top = node;
	}
	else
	{
		l->bottom->prev = node;
		node->next = l->bottom;
	}
	l->bottom = node;
	l->size++;
}

/*
 * Removes the bottom node from the list and returns the data it contained
 */
char pop(LinkedList* l)
{
	Node* node = l->bottom;
	if (node != NULL) // Ensure that the list is not empty
	{
		// Save the data of the node to return
		char c = node->data;

		// Update the bottom node of the list. If the list only had one item, it is now empty, so also null the top node.
		l->bottom = node->next; // may be null
		if (node == l->top) l->top = NULL;

		// Free and null the popped node
		free(node);
		node = NULL;
		l->size--;
		return c;
	}
	return 0; // This is reached if the list is empty
}

/*
 * Returns the data stored in the bottom node of the list without removing it
 */
char peek(LinkedList* l)
{
	if (l->bottom != NULL) return l->bottom->data;
	else return 0;
}

/*
 * Like pop(LinkedList), but returns and removes from the *top* of the list.
 */
char dequeue(LinkedList* l)
{
	Node* node = l->top;

	if (node != NULL) // Ensure that the list is not empty
	{
		// Save the data of the node to return
		char c = node->data;

		// Update the top of the list to this node's previous. If the list is now empty, set the bottom to null
		l->top = node->prev;
		if (node == l->bottom) l->bottom = NULL;

		// Free the memory
		free(node);
		node = NULL;
		l->size--;
		return c;
	}
	return 0;
}

/*
 * Empty the list, making sure to properly dispose of each element
 * Note that simply reassigning top and bottom to both be null wouldn't free the allocated memory
 * and could cause a leak
 */
void clear(LinkedList* l)
{
	while (l->size > 0) pop(l);
}

int size(LinkedList* l)
{
	return l->size;
}
