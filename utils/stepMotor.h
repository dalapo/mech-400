#ifndef STEPMOTOR_H_INCL
#define STEPMOTOR_H_INCL

#include<avr/io.h>

void step();
void stepReverse();
void rotate(long steps);
void rotateDeg(long deg);
void rotateAccel(long steps);
void setPos(int pos); // pos is in steps
void goToPos(int pos);
void goToPosAccel(int pos);
void resetMotor();
void setRate(int s);
void initAccelProfile();
// void setmsToMax(int ms);
// int getmsToMax();

#endif
