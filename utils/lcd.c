#include <avr/io.h>
#include <inttypes.h>

#ifndef F_CPU
//	#define F_CPU 12000000UL
//	#define F_CPU 1000000UL		//Frequency of uC - AT90USB1287
   #define F_CPU 16000000UL		//Frequency of uC - ATmega2560
#endif

#include <util/delay.h>

#include "lcd.h"



volatile char LCDData = 0x0F;
volatile char LCDDDR = 0xFF;

char getLCDPin()
{
	return ((PING >> 1) & 0x1F) | ((PINE << 2) & 0x20) | ((PINH << 3) & 0xC0);
}

void setLCDData(char data)
{
	LCDData = data;
	PORTG &= 0xC1;
	PORTE &= 0xF7;
	PORTH &= 0xE7; 
	PORTG |= (data & 0x1F) << 1; // Bits 0-4 left-shifted once to G1-5
	PORTE |= (data & 0x20) >> 2; // Bit 5 right-shifted twice to E3
	PORTH |= (data & 0xC0) >> 3; // Bits 6/7 right-shifted three times to H3/H4
}

void setLCDDDR(char ddr)
{
	LCDDDR = ddr;
	DDRG &= 0b11000001; 
	DDRE &= 0b11110111;
	DDRH &= 0b11100111;
	DDRG |= (ddr & 0b00011111) << 1; // Bits 0-4 left-shifted once to G1-5
	DDRE |= (ddr & 0b00100000) >> 2; // Bit 5 right-shifted twice to E3
	DDRH |= (ddr & 0b11000000) >> 3; // Bits 6/7 right-shifted three times to H3/H4
}

// 0 - 0x01
// 1 - 0x02
// 2 - 0x04
// 3 - 0x08
// 4 - 0x10
// 5 - 0x20
// 6 - 0x40
// 7 - 0x80
//   Code modified again to use: RS - H5; RW - G1; E - H6; DB4 - G5; DB5 - E3; DB6 - H3; DB7 - H4; A - GND; K - 5V

void LCDByte(uint8_t c,uint8_t isdata)
{
//Sends a byte to the LCD in 4bit mode
//cmd=0 for data
//cmd=1 for command


//NOTE: THIS FUNCTION RETURS ONLY WHEN LCD HAS PROCESSED THE COMMAND

uint8_t hn,ln;			//Nibbles
// uint8_t temp;

hn = c & 0xF0;
ln = (c & 0x0F) << 4;
// hn=c>>4;
// ln=(c & 0x0F);

if(isdata==0)
	CLEAR_RS();
else
	SET_RS();

_delay_us(0.5);		//tAS

SET_E();

//Send high nibble

setLCDData(hn);

_delay_us(1);			//tEH

//Now data lines are stable pull E low for transmission

CLEAR_E();

_delay_us(1);

//Send the lower nibble
SET_E();

// temp=(LCDData & 0XF0)|(ln);
// temp = (LCDData & 0x0F) | ln;
setLCDData(ln);

_delay_us(1);			//tEH
						//Do not wait too long, 1 us is good

//SEND

CLEAR_E();

_delay_us(1);			//tEL
setLCDData(0x00);
LCDBusyLoop();
_delay_ms(1); // Don't ask why we need this but apparently we do
}

void LCDBusyLoop()
{
	//This function waits till lcd is BUSY

	uint8_t busy,status=0x00,temp;

	//Change Port to input type because we are reading data
	DDRH &= ~0x10;
//	setLCDDDR(0x00);

	//change LCD mode
	SET_RW();		//Read mode
	CLEAR_RS();		//Read status

	//Let the RW/RS lines stabilize
	_delay_us(1);		//tAS
	
	do
	{

		SET_E();

		//Wait tDA for data to become available
		_delay_us(0.5);
		
		status = PINH;
		status &= 0x10;
		_delay_us(0.5);
		
		//Pull E low
		CLEAR_E();
		_delay_us(2);	//tEL
/*		
		SET_E();
		_delay_us(0.5);
		
		temp = PINH;
		temp &= 0x10;
//		temp = PINH & 0x10;

		status=status|temp;

		busy=status & 0b00010000;

		_delay_us(0.5);
		
		CLEAR_E();
		_delay_us(1);	//tEL
		
		*/
	}while(status);

	CLEAR_RW();		//write mode
	//Change Port to output
	//LCD_DATA_DDR|=0x0F;
//	setLCDDDR(LCDDDR | 0xF0);
//	setLCDDDR(0xFF);
	DDRH |= 0x10;

}

void InitLCD(char style)
{
	/*****************************************************************
	
	This function Initializes the lcd module
	must be called before calling lcd related functions

	Arguments:
	style = LS_BLINK,LS_ULINE(can be "OR"ed for combination)
	LS_BLINK :The cursor is blinking type
	LS_ULINE :Cursor is "underline" type else "block" type

	*****************************************************************/
	
	//After power on Wait for LCD to Initialize
	_delay_ms(30);
		
	//Set IO Ports
	setLCDDDR(0xF0);
//	setLCDDDR(LCDDDR | 0xF0);
	LCD_E_DDR|=(1<<LCD_E_POS);
	LCD_RS_DDR|=(1<<LCD_RS_POS);
	LCD_RW_DDR|=(1<<LCD_RW_POS);

	setLCDData(LCDData & 0x0F);
	CLEAR_E();
	CLEAR_RW();
	CLEAR_RS();

	//Set 4-bit mode
	_delay_us(0.3);	//tAS
	
	SET_E();
	setLCDData(LCDData | 0x20);
	_delay_us(2);
	
	CLEAR_E();
	_delay_us(1);
		
	//Wait for LCD to execute the Functionset Command
	LCDBusyLoop();                                    //[B] Forgot this delay

	//Now the LCD is in 4-bit mode

	LCDCmd(0x0C|style);	//Display On
	_delay_ms(1);
	LCDCmd(0x28);			//function set 4-bit,2 line 5x7 dot format
	_delay_ms(1);
	setLCDData(0x00);
}

// Convenience method to initialize the LCD
void initLCD()
{
	InitLCD(0x03);
}

void LCDWriteString(const char *msg)
{
	/*****************************************************************
	
	This function Writes a given string to lcd at the current cursor
	location.

	Arguments:
	msg: a null terminated string to print


	*****************************************************************/
 while(*msg!='\0')
 {
	LCDData(*msg);
	msg++;
 }
}

// This method was never finished. Or needed.
// We might need it now. Still TBD.
void LCDWriteFloat(float val)
{
	int whole = (int)val;
	float part = val - whole;
	LCDWriteInt(whole, -1);
	LCDWriteString(".");
//	long l = *(long*)&part; // evil floating point bit level hacking
//	l &= 0x807FFFFF; // strip away bits 30:23 (the exponent), leaving only the sign and mantissa
}

// I improved on this method by removing the 5 digit limit and fixing negative number implementation.
void LCDWriteInt(long val,int field_length)
{
	/***************************************************************
	This function writes a integer type value to LCD module

	Arguments:
	1)int val	: Value to print

	2)unsigned int field_length :total length of field in which the value is printed
	(-1 to match number length)

	****************************************************************/

	if (val == 0)
	{
		if (field_length == -1) field_length = 1;
		else
		{
			for (int i=0; i<field_length; i++)
			{
				LCDData('0');
			}
		}
		return;
	}
	char isNeg = 0;
	if (val < 0)
	{
		isNeg = 1;
		val = -val;
	}
	if (field_length == -1)
	{
		long cpy = val;
		int k = 0;
		while (cpy)
		{
			k++;
			cpy=cpy/10;
		}
		if (k == 0) field_length = 1; else field_length = k;
	}
	char* str = malloc(sizeof(char) * field_length);
	int idx = field_length-1;
	while(val)
	{
	str[idx]=val%10;
	val=val/10;
	idx--;
	}
	if(isNeg) LCDData('-');
	for(int i=0; i<field_length;i++)
	{
		LCDData(48+str[i]);
	}
	free(str);
}
void LCDGotoXY(uint8_t x,uint8_t y)
{
 if(x<40)
 {
  if(y) x|=0b01000000;
  x|=0b10000000;
  LCDCmd(x);
  }
}





