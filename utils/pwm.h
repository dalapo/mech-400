#ifndef PWM_H_INCL
#define PWM_H_INCL

#include<avr/io.h>
#include<avr/interrupt.h>
#include"misc.h"

// 16-bit wavegen options
#define WGM_NORMAL_16 0x00
#define WGM_PWMPC8_16 0x01
#define WGM_PWMPC9_16 0x02
#define WGM_PWMPC10_16 0x03
#define WGM_MCTC_16 0x04
#define WGM_PWMF8_16 0x05
#define WGM_PWMF9_16 0x06
#define WGM_PWMF10_16 0x07
#define WGM_PWMPFI_16 0x08
#define WGM_PWMPFO_16 0x09
#define WGM_PWMPCI_16 0x0A
#define WGM_PWMPCO_16 0x0B
#define WGM_CTC_16 0x0C
// 0x0D is reserved
#define WGM_PWMFI_16 0x0E
#define WGM_PWMFO_16 0x0F

// 8-bit wavegen options
#define WGM_NORMAL 0x00 // Normal operation, no PWM
#define WGM_PCPWM 0x01 // PWM, Phase correct
#define WGM_0CTC 0x02 // CTC
#define WGM_FPWM 0x03 // Fast PWM, 0 to OxFF, signals at OCRA
// 0x04 is reserved
#define WGM_PCPWMO 0x05 // PWM, Phase correct, OCRA to 0xFF, signals at OCRA (I think)
// 0x06 is reserved
#define WGM_FPWMO 0x07 // Fast PWM, OCRA to 0xFF, signals at OCRA (I think)
void setWaveGen(int timer, char wave);
void setDutyCycle(int tmr, int sub, uint val);
void initPWM(int timer);
char getWaveGen();
uint getDutyCycle(int timer, int sub);

#endif
