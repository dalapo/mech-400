#include <avr/io.h>
#include <stdlib.h>

#ifndef F_CPU
//	#define F_CPU 12000000UL
//	#define F_CPU 1000000UL		//Frequency of uC - AT90USB1287
   #define F_CPU 16000000UL		//Frequency of uC - ATmega2560
#endif

#include <util/delay.h>

#include "myutils.h"

#ifndef _LCD_H
#define _LCD_H
/*_________________________________________________________________________________________*/

/************************************************
	LCD CONNECTIONS
   Uses PORTD.7 - Strobe, PORT.6 - RS, PORT.5 - RW, DataNibble PORTC.4 - PORTC.7
   Code Modified to use PORTC Only

   Code modified again to use: RS - H5; RW - G1; E - H6; DB4 - G5; DB5 - E3; DB6 - H3; DB7 - H4; A - GND; K - 5V
*************************************************/

#define LCD_E H 		//Enable OR strobe signal
#define LCD_E_POS	PH6	//Position of enable in above port

#define LCD_RS H	
#define LCD_RS_POS 	PH5

#define LCD_RW G
#define LCD_RW_POS 	PG0


//************************************************

#define LS_BLINK 0B00000001
#define LS_ULINE 0B00000010



/***************************************************
			F U N C T I O N S
****************************************************/



void InitLCD(char style);
void initLCD();
void LCDWriteString(const char *msg);
void LCDWriteInt(long val, int field_length);
void LCDWriteFloat(float val);
void LCDGotoXY(uint8_t x,uint8_t y);

//Low level
void LCDByte(uint8_t,uint8_t);
#define LCDCmd(c) (LCDByte(c,0))
#define LCDData(d) (LCDByte(d,1))

void LCDBusyLoop();





/***************************************************
			F U N C T I O N S     E N D
****************************************************/


/***************************************************
	M A C R O S
***************************************************/
#define LCDClear() LCDCmd(0b00000001)
#define LCDHome() LCDCmd(0b00000010);
#define LCD_DATA_PORT 	PORT(LCD_DATA)
#define LCD_E_PORT 	PORT(LCD_E)
#define LCD_RS_PORT 	PORT(LCD_RS)
#define LCD_RW_PORT 	PORT(LCD_RW)

#define LCD_DATA_DDR 	DDR(LCD_DATA)
#define LCD_E_DDR 	DDR(LCD_E)
#define LCD_RS_DDR 	DDR(LCD_RS)
#define LCD_RW_DDR 	DDR(LCD_RW)

#define LCD_DATA_PIN	PIN(LCD_DATA)

#define SET_E() (LCD_E_PORT|=(1<<LCD_E_POS))
#define SET_RS() (LCD_RS_PORT|=(1<<LCD_RS_POS))
#define SET_RW() (LCD_RW_PORT|=(1<<LCD_RW_POS))

#define CLEAR_E() (LCD_E_PORT&=(~(1<<LCD_E_POS)))
#define CLEAR_RS() (LCD_RS_PORT&=(~(1<<LCD_RS_POS)))
#define CLEAR_RW() (LCD_RW_PORT&=(~(1<<LCD_RW_POS)))


#define LCDWriteStringXY(x,y,msg) {\
 LCDGotoXY(x,y);\
 LCDWriteString(msg);\
}

#define LCDWriteIntXY(x,y,val,fl) {\
 LCDGotoXY(x,y);\
 LCDWriteInt(val,fl);\
}
/***************************************************/




/*_________________________________________________________________________________________*/
#endif






