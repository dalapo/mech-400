#ifndef MENU_H_INCL
#define MENU_H_INCL

#include<avr/io.h>
#include"misc.h"

typedef struct MenuLevel // Implemented as an arbitrary arity tree
{
	int level;
	int option;
	int childCount;
	char* name;
	struct MenuLevel* parent; // If null, we are at the top level
	struct MenuLevel** children; // Array of pointers to other GuiLevels
	void (*action)(struct MenuLevel*);
//	void* action(struct MenuLevel*);
} MenuLevel;

volatile MenuLevel* menu = 0;
volatile int prevOption;
volatile int curOption;
volatile bool hasChanged;


// TODO: Create an "initMenu" function that returns the menu pointer
MenuLevel* createEntry();
void allocateChildren(MenuLevel*, int);
// bool assignChild(MenuLevel*, MenuLevel*);
// bool createChild(MenuLevel*);
void destroyMenu(MenuLevel*);
void back();
void selectOption();
void cycle();
volatile MenuLevel* getMenuPointer();

#endif
