#include<avr/io.h>
#include"timer.h"

/*
 * Utility file for the timer.
 * Contains convenience methods for initializing and setting the resolution of the timer, as well as the mTimer method.
 *
 * A possibility that is not implemented is using an interrupt to run a function in x amount of time, but without completely
 * pausing program execution like mTimer does.
 */
void initTimer()
{
	CLKPR = 0x80;
	CLKPR = 0x01;
	setRes(0x03E8);
	TCCR1B |= _BV(CS11);
}

void setRes(int res)
{
	OCR1A = res;
}

void startTimer() // Debounce timer
{
	TIMSK4 |= 0x02;
	TCNT4 = 0x0000;
}

// count is the delay in ms
void mTimer(long count)
{
	long i = 0;
	TCCR1B |= _BV(WGM32);
	TCNT1 = 0x0000;
	TIFR1 |= _BV(OCF1A);
	
	while (i < count)
	{
		if ((TIFR1 & 0x02) == 0x02)
		{
			TIFR1 |= _BV(OCF1A);
			i++;
		}
	}
}	
