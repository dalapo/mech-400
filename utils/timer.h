#ifndef __TIMER_H
#define __TIMER_H

#include<avr/io.h>
#define TIMER_RES OCR3A

void initTimer();
void startTimer();
void setRes(int res);
void mTimer(long delay);

#endif
