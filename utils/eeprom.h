#ifndef EEPROM_H_INCL
#define EEPROM_H_INCL

#include<avr/io.h>
#include "misc.h"

void flashWriteInt(uint addr, int16_t data);
void flashWriteChar(uint addr, char data);
void flashWriteString(uint addr, const char* data);

int flashReadInt(uint addr);
char flashReadChar(uint addr);
char* flashReadString(uint addr);

#endif
