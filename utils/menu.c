#include<avr/io.h>
#include<stdlib.h>
#include "menu.h"
#include "misc.h"

// Menu mockup
// Left button: Cycle
// Center button: Select
// Right button: Back
//

volatile MenuLevel* getMenuPointer()
{
	if (menu == NULL) menu = createEntry();
	return menu;
}

// Create a new menu entry and initialize all its settings to their defaults
// It is expected that these settings will be overwritten

MenuLevel* createEntry()
{
	MenuLevel* level = malloc(sizeof(MenuLevel));
	level->parent = NULL;
	level->action = NULL;
	level->childCount = 0;
	level->level = 0;
	return level;
}

// Create a number (= size) of sub-options under parent
void allocateChildren(MenuLevel* parent, int size)
{
	parent->childCount = size;
	parent->children = malloc(size * sizeof(MenuLevel*));
	for (int i=0; i<size; i++)
	{
		MenuLevel* child = createEntry();
		child->parent = parent;
		child->level = parent->level + 1;
		child->option = i;
		parent->children[i] = child;
	}
}


// Depth-first menu destruction. DO NOT FORGET TO CALL THIS WHEN YOU NO LONGER NEED THE MENU
void destroyMenu(MenuLevel* top)
{
	for (int i=0; i<top->childCount; i++)
	{
		destroyMenu(*(top->children + i));
	}

	free(top->children);
	free(top);
	top = NULL;
}

// TODO: Rewrite these to accept a menu pointer pointer as an argument
// to avoid needing a global variable
void back()
{
	if (menu->parent != NULL)
	{
		menu = menu->parent;
		curOption = prevOption;
	}
}

void selectOption()
{
	if (menu->children[curOption]->childCount > 0)
	{
		menu = menu->children[curOption];
		prevOption = curOption;
		curOption = 0;
	}
	else if (menu->children[curOption]->action != NULL) // Do nothing if no action has been assigned
	{
		// I wonder what happens if a 2560 processor segfaults
		(*(menu->children[curOption]->action))(menu->children[curOption]);
	}
}

void cycle()
{
	curOption = (curOption + 1) % menu->childCount;
}
