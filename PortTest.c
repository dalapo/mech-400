#include<avr/io.h>
#include"utils/timer.h"

int main(int argc, char** argv)
{
	initTimer();
	for (int i=0; i<7; i++)
	{
		_SFR_IO8((i*3)+1) = 0xFF;
		_SFR_IO8((i*3)+2) = 0xFF;
		if (i != 0) _SFR_IO8((i*3)-1) = 0x00;
		mTimer(1000);
	}
	for (int i=0x100; i<0x104; i++)
	{
		_SFR_IO8((i*3)+1) = 0xFF;
		_SFR_IO8((i*3)+2) = 0xFF;
		if (i != 0x100) _SFR_IO8((i*3)-1) = 0x00;
		mTimer(1000);
	}
	return 0;
}
