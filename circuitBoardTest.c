#include<avr/io.h>
#include"utils/timer.h"
#include"utils/misc.h"

int main(int argc, char** argv)
{
	DDRL = 0xFF;
	initTimer();
	PORTL |= 0x80;
	while (true)
	{
		PORTL ^= 0xFF;
		mTimer(1000);
	}
}
