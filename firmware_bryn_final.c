// Code for MECH 400 Capstone project.
// Written for the Atmel ATMega2560 microcontroller.
// Controls the handheld cloud chamber.

#include<avr/io.h>
#include<math.h>
#include"utils/adc.h"
#include"utils/misc.h"
#include"utils/interrupts.h"
#include"utils/lcd.h"
#include"utils/timer.h"
#include"utils/pwm.h"

/*
 * PORTS:
 * Reset button: N_RESET (30)
 *
 * L: Output
 * L6: User LED 1
 * L7: User LED 2
 * L4: LCD (No PWM)
 * L3: Chamber LEDs
 * L1: HV Converter
 * L0: 12V supply (enable first)
 *
 * H: Output
 * H6: LCD_E
 * H5: LCD_RS
 * H4: LCD_DB7
 * H3: LCD_DB6
 *
 * B: Input
 * B7: Button 3
 * B6: Button 2
 * B5: Button 1
 * B4: 12V Error
 * B3: ICSP_MISO
 * B2: ICSP_MOSI
 * B1: ICSP_SCK
 * B0: Floating
 *
 * D: Serial stuff (less important)
 * D1: SDA
 * D0: SCL
 *
 * E: Output
 * E5: Heater
 * E4: Fan (PWM, OC3B)
 * E3: LCD DB5
 *
 * F: Input
 * F2: Heatsink monitor (ADC)
 * F1: Battery monitor (ADC)
 * F0: TEC monitor (ADC)
 *
 * G: Output
 * G5: LCD DB4
 * G4: LCD DB3 (ignore)
 * G3: LCD DB2 (ignore)
 * G2: LCD DB1 (ignore)
 * G1: LCD DB0 (ignore)
 *
 * PWMs:
 *
 * LCD Backlight: L4, OC5B, Timer 5
 * Heater: E5, OC3C, Timer 3
 * Fan: E4, OC3B, Timer 3
 * Chamber LEDs: L3, OC5A, Timer 5
 * 
 * Timers:
 * Timer 0: Main loop sync
 * Timer 1: mTimer
 * Timer 3: PWM
 * Timer 4: Debounce
 * Timer 5: PWM
 */
// Constants and globals

const int B = 3988;
const float V_IN = 5.0;
const int DEBOUNCE_LENGTH = 50;
volatile char flags = 0x00;
volatile bool debounce = false;
volatile bool isRunning = true;
volatile int coolTime = 0;

/*
 * Flag bits:
 * 0: Cloud chamber LEDs on/off
 * 1:
 * 2:
 * 3:
 * 4:
 * 5:
 * 6:
 * 7:
 */

// Chamber LEDs
void onFirstButtonPressed()
{
	if (!debounce)
	{
		PORTL ^= 0x02;
		debounce = true;
		startTimer();
	}
}

// Heater
void onSecondButtonPressed()
{
	if (!debounce)
	{
		PORTC ^= 0x02; // Toggle heater
		debounce = true;
		startTimer();
	}
}

// HV field
void onThirdButtonPressed()
{
	if (!debounce)
	{
		PORTC ^= 0x04; // Toggle HV field;
		debounce = true;
		startTimer();
	}
}

// Temperature is measured using a thermistor. Need to check circuit diagram for details.
// Cold side constant resistance: 130k (zero at -35C)
// Hot side constant resistance: 5k

// Returns temperature in kelvin
float convertThermistor(int reading)
{
	float V_out = (V_IN * reading)/1024.0f;
	float T_inverse = 0.003354f + (log(V_out) - log(V_IN - V_out))/(float)B; // 0.003354 = 1/298.15
	return 1.0 / T_inverse;
}

// Converts error hex-code to string descriptor
const char* ErrorByte2str(char byte) 
{
	switch (byte) //Compare byte to expected error values, return matching result
	{
		case 0x00:
			return "No Error";
		
		case 0x01:
			return "Low Battery";
			
		case 0x02:
			return "12V Error"
			
		case 0x03:
			return "Cooldown Error";
			
		case 0x04:
			return "Heatsink Overheat"
			
		default:
			return "Unrecognized Error" //If error code does not match, error is unknown/unexpected
	}
}

#define NO_ERROR 0x00
#define LOW_BATTERY 0x01
#define 12V_ERROR 0x02
#define COULD_NOT_COOL 0x03
#define OVERHEAT 0x04
// Cause:
// 0x00: Normal shutdown, no error
// 0x01: Low battery
// 0x02: 12V error
// 0x03: Couldn't reach target temp
// 0x04: Heatsink overheat
void shutdown(char cause)
{
	// Shutdown sequence:
	// Shut off everything ending with 12V regulator (L0)
	
	PORTD = 0x00; //SDA & SCL
	PORTK = 0x00; //Unsure of sequence for K/J, TBC with Nick
	PORTJ = 0x00; //Digital expansion similar to K
	PORTE = 0x08; //Heat, Fan, leave LCD DB5 on
	PORTF = 0x00; //Heatsink, Battery, TEC monitors
	PORTB = 0XE0; //Leave SW1-SW3 on so that buttons still function
	PORTL = 0x80; //HV Converter, 12V supply, LCD backlight, leave LED2 on
	
	// Go into low power sleep mode (to be written); hold control until reboot
	
	// Display cause on LCD
	LCDClear();
	LCDWriteString(ErrorByte2str(cause)); //Converts byte to error description
	
	isRunning = false;
	while (true)
	{
		// Stall the program until it's reset
	}
}

// Low battery mode: shut down everything possible (that is, everything connected to 5V)
// - LCD backlight
// - LEDs other than power
// - Fan, Peltier, heatsink, etc
// Shutdown is essentially lower power mode?

int main(int argc, char** argv)
{
	// Initialize microcontroller basics: Timer, PWM, interrupts, ADC, etc
	initTimer();
	initLCD(0x00); // Init LCD, no cursor
	initADC();
	initInterrupts(0x07, 0x00); // Activate the first 3 interrupts, all falling edge
				    // 0x07 = 0b00000111
	initPWM();
	setWaveGen(WGM_FPWM);

	assignFunction(0, onFirstButtonPressed);
	assignFunction(1, onSecondButtonPressed);
	assignFunction(2, onThirdButtonPressed);

	cli();
	// 1-second main loop res
	TCCR1B |= _BV(CS11) | _BV(WGM32);
	OCR1A = 1000L; // Interrupt every ms
	TCNT1 = 0x0000;
	TIMSK1 |= 0x02;

	TCCR7B |= _BV(CS11) | _BV(WGM32);
	OCR7A = 20000L; // Interrupt every 20 ms
	TCNT7 = 0x0000;
	TIMSK1 |= 0x02;
	sei();

	// Initialize data directions
	DDRB = 0x00; // Buttons and 12V error
	DDRD = 0xFF; // SDA/SCL, not super important immediately
	DDRE = 0xFF; // Heater, fan, LCD DB5
	DDRF = 0x00; // Heat sink, TEC, and battery monitors
	DDRG = 0xFF; // LCD DB0-4 and RW
	DDRH = 0xFF; // LCD DB6-7, RS, E
	DDRL = 0xFF; // LEDs, HV converter, 12V supply
	
	// Check battery voltage, shutdown if less than 14.8V
	int battery = getPortADC10Bit(0x03);
	if (battery < 768) shutdown(LOW_BATTERY);

	PORTL = 0x3F; // Turn on all 12V stuff except the user LEDs
	// Otherwise initialize everything and start TEC cooling sequence
	
	while (true) // Main loop
	{
		float tecTemp = convertThermistor(getPortADC10Bit(0x01)) - 273.15;
		float heatsinkTemp = convertThermistor(getPortADC10Bit(0x02)) - 273.15;
		battery = getPortADC10Bit(0x03); // goes from 0 to 1024

		if (PINB & 0x10 == 0) shutdown(12V_ERROR);
		if (battery < 768) shutdown(LOW_BATTERY);

		LCDClear(); // May need to reduce flickering
		
		if (tecTemp > -25)
		{
			LCDWriteString("Cooling");
			coolTime++;
			if (coolTime > 120) shutdown(COULD_NOT_COOL);
		}
		else
		{
			coolTime = 0;
			LCDWriteString("Ready");
		}
		LCDGotoXY(0,1);
		LCDWriteString("Battery: ");
		for (int i=0; i<4; i++)
		{
			if (battery >= (i*64)+704) LCDData(0xFF);
			else LCDWriteString("_");
		}

		// Wait for the timer to catch up
		while (flags & 0x01 == 0) {}
		flags &= ~0x01;
	}
	return 0;
}

volatile int ticks = 0;
ISR(TIMER1_COMPA_vect)
{
	if (++ticks >= 1000)
	{
		ticks = 0;
		flags |= 0x01;
	}
}

volatile int debounceMS = 0;
ISR(TIMER4_COMPA_vect)
{
	if (++debounceMS >= DEBOUNCE_LENGTH)
	{
		debounce = false;
		debounceMS = 0;
		TIMSK4 &= ~0x02;
	}
}
