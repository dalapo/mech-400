#include<avr/io.h>
#include"menu.h"

/*
 * MENU TREE STRUCTURE
 *
 * Top level
 * -Options
 * --Chamber LEDs
 * ---Increase
 * ---Decrease
 * --Fan speed
 * ---Increase
 * ---Decrease
 * --Heater power
 * ---Increase
 * ---Decrease
 * -Save/Load
 * --Save
 * --Load
 * -Sleep
 * --Confirm
 * -Shutdown
 * --Confirm
 */

void initMenu()
{
	MenuLevel* top = getMenuPointer();
	allocateChildren(top, 4); // Top level, 4 choices
	allocateChildren(top->children[0], 3); // Options: LEDs, Fan speed, Heater power
	for (int i=0; i<top->children[0]->childCount, i++)
	{
		allocateChildren(top->children[0]->children[i], 2); // Increase/decrease
	}
	allocateChildren(top->children[1], 2); // Save/Load (Save, Load)
	allocateChildren(top->children[2], 1); // Confirm
	allocateChildren(top->children[3], 1); // Confirm
}
