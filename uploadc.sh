#!/bin/bash

rm /tmp/prgm
rm /tmp/prgm.hex
avr-gcc -mmcu=atmega2560 ${1} utils/* -O -o /tmp/prgm
if [[ -f /tmp/prgm ]]; then
	echo "Uploading"
	avr-objcopy -O ihex -R .eeprom /tmp/prgm /tmp/prgm.hex
	avrdude -c wiring -P /dev/ttyACM0 -p m2560 -D -U flash:w:/tmp/prgm.hex:i
fi
