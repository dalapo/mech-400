#!/bin/bash

# rm /tmp/prgm
# rm /tmp/prgm.hex
avr-gcc -mmcu=atmega2560 ${1} utils/* -O -o "${2}.out"
avr-objcopy -O ihex -R .eeprom "${2}.out" "${2}.hex"
