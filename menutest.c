#include<avr/io.h>
#include<stdlib.h>
#include"utils/menu.h"
#include"utils/timer.h"
#include"utils/lcd.h"
#include"utils/interrupts.h"
#include"utils/adc.h"
#include"utils/misc.h"

volatile char lcdState = 0x00; // 0 = show menu; other = show other stuff
volatile bool debounce = false;
volatile int debounceMS = 0;
const int DEBOUNCE_TIME = 50;

void flashLEDs(MenuLevel* lvl)
{
	PORTC = 0xFF;
	mTimer(200);
	PORTC = 0x00;
}

void leftWave(MenuLevel* lvl)
{
	for (int i=0; i<8; i++)
	{
		PORTC = 0x80 >> i;
		mTimer(100);
	}
	PORTC = 0x00;
}

void rightWave(MenuLevel* lvl)
{
	for (int i=0; i<8; i++)
	{
		PORTC = 0x01 << i;
		mTimer(100);
	}
	PORTC = 0x00;
}

void disco(MenuLevel* lvl)
{
	PORTC = 0xAA; // 0b10101010
	for (int i=0; i<8; i++)
	{
		mTimer(100);
		PORTC ^= 0xFF;
	}
	PORTC = 0x00;
}

void backlightOff(MenuLevel* lvl)
{
	PORTL &= ~0x10;
}

void backlightOn(MenuLevel* lvl)
{
	PORTL |= 0x10;
}

void adcReading(MenuLevel* lvl)
{
	lcdState = 0x01;
}

void Int0()
{
//	PORTL |= 0x80;
//	mTimer(100);
//	PORTL &= ~0x80;
	if (!debounce && lcdState == 0)
	{
		debounce = true;
		cycle();
		hasChanged = true;
		startTimer();
	}
}

void Int1()
{
//	PORTL |= 0x80;
//	mTimer(100);
//	PORTL &= ~0x80;

	if (!debounce && lcdState == 0)
	{
		debounce = true;
		selectOption();
		hasChanged = true;
		startTimer();
	}
}

void Int2()
{
//	PORTL |= 0x80;
//	mTimer(100);
//	PORTL &= ~0x80;


	if (!debounce)
	{
		debounce = true;
		if (lcdState == 0) back();
		else lcdState = 0;
		hasChanged = true;
		startTimer();
	}
}

int main(int argc, char** argv)
{
	// Yes, creating a menu this way is tedious.
	// No, there isn't a better way to do it.
	MenuLevel* top = createEntry();
	menu = top;
	top->name = "Top level";
	allocateChildren(top, 3);
//	for (int i=0; i<3; i++)
//	{
//		assignChild(top, malloc(sizeof(MenuLevel)));
//	}
	MenuLevel* led = top->children[0];
	MenuLevel* lcd = top->children[1];
	MenuLevel* other = top->children[2];
	led->name = "LED options";
	lcd->name = "LCD options";
	other->name = "ADC reading";

	allocateChildren(led, 4);
	MenuLevel* flash = led->children[0];
	MenuLevel* left = led->children[1];
	MenuLevel* right = led->children[2];
	MenuLevel* dis = led->children[3];

	flash->name = "Flash all";
	left->name = "Left wave";
	right->name = "Right wave";
	dis->name = "Disco!";
	flash->action = flashLEDs;
	left->action = leftWave;
	right->action = rightWave;
	dis->action = disco;
	allocateChildren(lcd, 2);
	lcd->children[0]->name = "Backlight on";
	lcd->children[1]->name = "Backlight off";
	lcd->children[0]->action = backlightOn;
	lcd->children[1]->action = backlightOff;
	other->action = adcReading;

	menu = top;

	initTimer();
	InitLCD(0x00);

	/*
	InitLCD(0x00);
	initInterrupts(0x07, 0x00);
	assignFunction(0, Int0);
	assignFunction(1, Int1);
	assignFunction(2, Int2);

	*/
	LCDCmd(0x0C);
	cli();
	PCICR |= _BV(PCIE0);
	PCMSK0 = 0xE0; // Enable PCINT5,6,7
	DDRB &= ~0xE0; // Enable pullup
	PORTB |= 0xE0;

	TCCR4B |= _BV(WGM32) | _BV(CS11);
	OCR4A = 1000L;
	TCNT4 = 0x0000;
	TIMSK4 |= 0x02;
	sei();
	// SREG |= 0x80; // AAAAAAAAA
	LCDClear();
	initADC();
	// DDRD &= ~0x07;
	// PORTD |= 0x07;

	DDRL = 0xFF;
	PORTL |= 0x10; // Default to backlight on
	LCDWriteString("Menu Test");
	mTimer(1000);
	hasChanged = true;
	curOption = 0;
	while (true)
	{
		if (lcdState != 0x00)
		{
			LCDClear();
			LCDWriteInt(getADC10Bit(), -1);
			mTimer(200);
		}

		else if (hasChanged)
		{
			LCDClear();
			LCDWriteString(menu->name);
			LCDGotoXY(0,1);
			mTimer(10);
			LCDWriteString(menu->children[curOption]->name);
			hasChanged = false;
		}

	}

	return 0;
}

ISR(PCINT0_vect)
{
	char c = (PINB >> 5) & 0x07;
	switch (c)
	{
		case 0x06: // 0 low
			Int0();
			break;
		case 0x05: // 1 low
			Int1();
			break;
		case 0x03: // 2 low
			Int2();
			break;
		default:
			break;
			// Multiple buttons pressed; do nothing
	}
}

ISR(TIMER4_COMPA_vect)
{
	if (++debounceMS >= DEBOUNCE_TIME)
	{
		debounce = false;
		debounceMS = 0;
		TIMSK4 &= ~0x02;
	}
}
