#include<avr/io.h>
#include"utils/lcd.h"
#include"utils/timer.h"

// L1: HV
// L3: Chamber LEDs
// E4: Fan
// E5: Heater
int main(int argc, char** argv)
{
	DDRL = 0xFF;
	DDRE = 0xFF;
	DDRB = 0x00;
	PORTB = 0xE0;
	initTimer();
	initLCD();
	LCDClear();

	LCDWriteString("Hello World!");

	PORTL = 0xDB;
	PORTE = 0x30;
	while (1)
	{
		/*
		if (update)
		{
		LCDClear();
		LCDWriteString("HV LEDs Fan");
		LCDGotoXY(0,1);
		if (PORTL & 0x01 != 0) LCDWriteString("O");
		else LCDWriteString("X");
		LCDWriteString("  ");
		if (PORTL & 0x08 != 0) LCDWriteString("O");
		else LCDWriteString("X");
		LCDWriteString("    ");
		if (PORTE & 0x10 != 0) LCDWriteString("O");
		else LCDWriteString("X");
		update = false;
		}
		*/
	}
	return 0;
}

/*
ISR(PCINT0_vect)
{

	char c = (PINB >> 5) & 0x07;
	switch (c)
	{
		case 0x06: // 0 low
			Int0();
			break;
		case 0x05: // 1 low
			Int1();
			break;
		case 0x03: // 2 low
			Int2();
			break;
		default:
			break;
			// Multiple buttons pressed; do nothing
	}
	update = true;
}
*/
