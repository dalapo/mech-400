// Code for MECH 400 Capstone project.
// Written for the Atmel ATMega2560 microcontroller.
// Controls the handheld cloud chamber.

#include<avr/io.h>
#include<math.h>
#include"utils/adc.h"
#include"utils/misc.h"
#include"utils/interrupts.h"
#include"utils/lcd.h"
#include"utils/timer.h"
#include"utils/pwm.h"
#include"utils/menu.h"
// #include"menu.h"

/*
 * PORTS:
 * Reset button: N_RESET (30)
 *
 * L: Output
 * L6: User LED 1
 * L7: User LED 2
 * L4: LCD (No PWM)
 * L3: Chamber LEDs
 * L1: HV Converter
 * L0: 12V supply (enable first)
 *
 * H: Output
 * H6: LCD_E
 * H5: LCD_RS
 * H4: LCD_DB7
 * H3: LCD_DB6
 *
 * B: Input
 * B7: Button 3
 * B6: Button 2
 * B5: Button 1
 * B4: 12V Error
 * B3: ICSP_MISO
 * B2: ICSP_MOSI
 * B1: ICSP_SCK
 * B0: Floating
 *
 * D: Serial stuff (less important)
 * D1: SDA
 * D0: SCL
 *
 * E: Output
 * E5: Heater
 * E4: Fan (PWM, OC3B)
 * E3: LCD DB5
 *
 * F: Input
 * F2: Heatsink monitor (ADC)
 * F1: Battery monitor (ADC)
 * F0: TEC monitor (ADC)
 *
 * G: Output
 * G5: LCD DB4
 * G4: LCD DB3 (ignore)
 * G3: LCD DB2 (ignore)
 * G2: LCD DB1 (ignore)
 * G1: LCD DB0 (ignore)
 *
 * PWMs:
 *
 * LCD Backlight: L4, OC5B, Timer 5
 * Heater: E5, OC3C, Timer 3
 * Fan: E4, OC3B, Timer 3
 * Chamber LEDs: L3, OC5A, Timer 5
 * 
 * Timers:
 * Timer 0: Main loop sync
 * Timer 1: mTimer
 * Timer 3: PWM
 * Timer 4: Debounce
 * Timer 5: PWM
 */
// Constants and globals

const int B = 3988;
const float V_IN = 5.0;
const int DEBOUNCE_LENGTH = 50;
const int MAX_COOL_TIME = 600;
// volatile bool debounce = false;
// volatile bool isRunning = true;
volatile int coolTime = 0;

#define LCD_NORMAL 0
#define LCD_MENU 1
#define LCD_SHUTDOWN 2
volatile char lcdState = 0;

/*
 * Flag bits:
 * 0: Main loop timer control
 * 1: Debounce
 * 2: Is running
 * 3: Is cooling
 * 4: Is sleeping
 */

#define FLG_MAINLOOP 0
#define FLG_DEBOUNCE 1
#define FLG_RUNNING 2
#define FLG_COOLING 3
#define FLG_SLEEPING 4
volatile char flags = 0x00;

void Int0()
{
	switch (lcdState)
	{
		case 0: // Standard
			break;
		case 1: // Menu
			cycle();
			break;
	}
}

void Int1()
{
	switch (lcdState)
	{
		case 0: // Standard
			lcdState = 1;
			while (getMenuPointer()->level > 0) back(); // Make sure we are at the top level
			break;
		case 1: // Menu
			select();
			break;
	}
}

void Int2()
{
	switch (lcdState)
	{
		case 0: // Standard
			break;
		case 1: // Menu
			if (getMenuPointer()->level == 0) lcdState = 0;
			else back();
			break;
	}
}

// Temperature is measured using a thermistor. Need to check circuit diagram for details.
// Cold side constant resistance: 130k (zero at -35C)
// Hot side constant resistance: 5k

// Returns temperature in kelvin
float convertThermistor(int reading)
{
	float V_out = (V_IN * reading)/1024.0f;
	float T_inverse = 0.003354f + (log(V_out) - log(V_IN - V_out))/(float)B; // 0.003354 = 1/298.15
	return 1.0 / T_inverse;
}

const char* ErrorByte2str(char byte) 
{
	switch (byte) //Compare byte to expected error values, return matching result
	{
		case 0x00:
			return "No Error";
		
		case 0x01:
			return "Low Battery";
			
		case 0x02:
			return "12V Error"
			
		case 0x03:
			return "Cooldown Error";
			
		case 0x04:
			return "Heatsink Overheat"
			
		default:
			return "Unrecognized Error" //If error code does not match, error is unknown/unexpected
	}
}

#define NO_ERROR 0x00
#define LOW_BATTERY 0x01
#define ERROR_12V 0x02
#define COULD_NOT_COOL 0x03
#define OVERHEAT 0x04
// Cause:
// 0x00: Normal shutdown, no error
// 0x01: Low battery
// 0x02: 12V error
// 0x03: Couldn't reach target temp
// 0x04: Heatsink overheat
void shutdown(char cause)
{
	// Shutdown sequence:
	// Shut off everything ending with 12V regulator (L0)
	
	PORTD = 0x00; //SDA & SCL
	PORTE = 0x08; //Heat, Fan, leave LCD DB5 on
	PORTF = 0x00; //Heatsink, Battery, TEC monitors
	PORTB = 0XE0; //Leave SW1-SW3 on so that buttons still function
	PORTL = 0x80; //HV Converter, 12V supply, LCD backlight, leave LED2 on
	
	// Go into low power sleep mode (to be written); hold control until reboot
	
	// Display cause on LCD
	LCDClear();
	LCDWriteString(ErrorByte2str(cause)); //Converts byte to error description
	
	isRunning = false;
	while (true)
	{
		// Stall the program until it's hard reset
	}
}

volatile char prevPorts[4];
void sleep()
{
	prevPorts[0] = PORTL;
	prevPorts[1] = PORTE;
	prevPorts[2] = PORTF;
	prevPorts[3] = PORTB;
	PORTL = 0x00; // Low power mode
	PORTE = 0x08;
	PORTF = 0x00;
	PORTB = 0xE0;
	setBit(&flags, FLG_SLEEPING);
}

void wake()
{
	PORTL = prevPorts[0];
	PORTE = prevPorts[1];
	PORTF = prevPorts[2];
	PORTB = prevPorts[3];
	clearBit(&flags, FLG_SLEEPING);
}

// Our eyes perceive light on a logarithmic scale. Therefore, the power level to the LEDs should be
// exponential instead of linear so that the light level *seems* linear.
char scaleLightLevel(int lvl) // Takes a level (0-9, nominative), returns e^(lvl) scaled from 0-255
{
	float raw = (exp(lvl) - 1) / exp(9); // Returns a number from 0 to 1 on an exponential curve
	uint scaled = raw * 255; // Scale 0-1 -> 0-255
	return (char)scaled; // Cast to char
}

volatile int ledLevel = 9;
void modify(MenuLevel* ptr)
{
	int mod = ptr->option == 0 ? 1 : -1;
	switch (ptr->parent->option)
	{
		case 0: // LED
			ledLevel = clamp(ledLevel + mod, 0, 9);
			setDutyCycle(5, 0, scaleLightLevel(ledLevel));
			break;
		case 1: // Fan speed
			int finalRes = clamp(getDutyCycle(3, 1) + mod*16, 0, 255);
			setDutyCycle(3, 1, finalRes);
			break;
		case 2: // Heater
			int finalRes = clamp(getDutyCycle(3, 2) + mod*16, 0, 255);
			break;
		case 3: // HV field
			if (mod > 0) setBit(&PORTL, 1);
			else clearBit(&PORTL, 1);
		default:
	}
}

/*
 * MENU TREE STRUCTURE
 *
 * Top level
 * -Options
 * --Chamber LEDs
 * ---Increase
 * ---Decrease
 * --Fan speed
 * ---Increase
 * ---Decrease
 * --Heater power
 * ---Increase
 * ---Decrease
 * -Save/Load
 * --Save
 * --Load
 * -Sleep
 * --Confirm
 * -Shutdown
 * --Confirm
 */


void initMenu()
{
	MenuLevel* menuPtr = getMenuPointer();
	menuPtr->name = "Main Menu";
	allocateChildren(menuPtr, 3); // Top level, 4 choices (Options, Save/Load, Sleep)
	menuPtr->children[0]->name = "Options";
	allocateChildren(menuPtr->children[0], 4); // Options: LEDs, Fan speed, Heater power, HV field
	for (int i=0; i<menuPtr->children[0]->childCount, i++)
	{
		MenuLevel* sub = menuPtr->children[0]->children[i];
		allocateChildren(sub, 2); // Increase/decrease
		sub->children[0]->action = modify;
		sub->children[1]->action = modify;
		if (i == 3)
		{
			sub->children[0]->name = "On";
			sub->children[1]->name = "Off";
		}
		else
		{
			sub->children[0]->name = "Increase";
			sub->children[1]->name = "Decrease";
		}
	}
	menuPtr->children[0]->children[0]->name = "Chamber brightness";
	menuPtr->children[0]->children[1]->name = "Fan speed";
	menuPtr->children[0]->children[2]->name = "Heater power";

	menuPtr->children[1]->name = "Save/Load";
	allocateChildren(menuPtr->children[1], 2); // Save/Load (Save, Load)
	menuPtr->children[1]->children[0]->name = "Save";
	menuPtr->children[1]->children[1]->name = "Load";

	menuPtr->children[2]->name = "Sleep";
	allocateChildren(menuPtr->children[2], 1); // Confirm
	menuPtr->childen[2]->children[0]->name = "Sleep? (Confirm)";
	menuPtr->children[2]->children[0]->action = sleep;
	curOption = 0;
}

void displayMenu()
{
	MenuLevel* menuPtr = getMenuPointer();
	LCDGotoXY(0,0);
	LCDWriteString(menuPtr->name);
	LCDGotoXY(0,1);
	LCDWriteString(menuPtr->children[curOption]->name);
}

// handleLCD() is called at the bottom of the main loop (once/sec) and whenever a button is pressed.
void handleLCD()
{
	LCDClear();
	switch (lcdState)
	{
		case LCD_NORMAL:
			// Standard display
			if (getBit(flags, FLG_COOLING)) LCDWriteString("Cooling");
			else LCDWriteString("Ready");
			LCDGotoXY(0,1);
			LCDWriteString("Battery: ");
			for (int i=0; i<4; i++)
			{
				if (battery >= (i*64)+704) LCDData(0xFF);
				else LCDWriteString("_");
			}
			break;
		case LCD_MENU:
			displayMenu();
			break;
		case LCD_SHUTDOWN:
			break;
	}
}

int main(int argc, char** argv)
{
	// Initialize microcontroller basics: Timer, PWM, interrupts, ADC, etc
	initTimer();
	initLCD(0x00); // Init LCD, no cursor
	LCDCmd(0x0C); // Resend the no-cursor command because of course we have to do that
	initADC();
	
	initPWM(3);
	initPWM(5);
	setWaveGen(3, WGM_PWMF8_16);
	setWaveGen(5, WGM_PWMF8_16);

	cli();
	// 1-second main loop res
	TCCR0B |= _BV(CS11) | _BV(WGM32);
	OCR0A = 1000L; // Interrupt every ms
	TCNT0 = 0x0000;
	TIMSK0 |= 0x02;

	TCCR4B |= _BV(CS11) | _BV(WGM32);
	OCR4A = 1000L; // Interrupt every ms
	TCNT4 = 0x0000;
	TIMSK4 |= 0x02;
	sei();

	// Initialize data directions
	DDRB = 0x00; // Buttons and 12V error
	DDRD = 0xFF; // SDA/SCL, not super important immediately
	DDRE = 0xFF; // Heater, fan, LCD DB5
	DDRF = 0x00; // Heat sink, TEC, and battery monitors
	DDRG = 0xFF; // LCD DB0-4 and RW
	DDRH = 0xFF; // LCD DB6-7, RS, E
	DDRL = 0xFF; // LEDs, HV converter, 12V supply
	
	// Interrupts
	cli();
	PCICR |= _BV(PCIE0);
	PCMSK0 = 0xE0; // Enable PCINT5,6,7
	PORTB |= 0xE0; // Enable pullup on buttons
	sei();

	// Check battery voltage, shutdown if less than 14.8V
	int battery = getPortADC10Bit(0x03);
	if (battery < 768) shutdown(LOW_BATTERY);

	PORTL = 0x17; // Turn on all 12V stuff except the user LEDs
	// Otherwise initialize everything and start TEC cooling sequence
	
	setDutyCycle(5, 0, 0xFF); // Chamber lights
	setDutyCycle(3, 1, 0xFF); // Fan
	setDutyCycle(3, 2, 0xFF); // Heater
	while (true) // Main loop
	{
		float tecTemp = convertThermistor(getPortADC10Bit(0x01)) - 273.15;
		float heatsinkTemp = convertThermistor(getPortADC10Bit(0x02)) - 273.15;
		battery = getPortADC10Bit(0x03); // goes from 0 to 1024

		if (tecTemp > 248) // -25C in kelvin
		{
			setBit(&flags, FLG_COOLING);
			coolTime++;
			if (coolTime > 600) shutdown(COULD_NOT_COOL);
		}
		else
		{
			coolTime = 0;
			clearBit(&flags, FLG_COOLING);
		}

		if (PINB & 0x10 == 0) shutdown(ERROR_12V); // 12V error shutdown
		if (battery < 768) shutdown(LOW_BATTERY); // Low battery shutdown

		handleLCD();
		// Wait for the timer to catch up
		while (!getBit(flags, FLG_MAINLOOP)) {}
		clearBit(&flags, FLG_MAINLOOP);
	}
	return 0;
}

ISR(PCINT0_vect)
{	
	if (!getBit(flags, FLG_DEBOUNCE))
	{
		if (getBit(flags, FLG_SLEEPING))
		{
			wake();
		}
		else
		{
		char c = (PINB >> 5) & 0x07;
		switch (c)
		{
			case 0x06: // 0 low
				Int0();
				break;
			case 0x05: // 1 low
				Int1();
				break;
			case 0x03: // 2 low
				Int2();
				break;
			default:
				break; // Multiple buttons pressed; do nothing
		}
		handleLCD();
		setBit(&flags, FLG_DEBOUNCE);
		startTimer();
		}
	}
}


volatile int ticks = 0;
ISR(TIMER1_COMPA_vect)
{
	if (++ticks >= 1000)
	{
		ticks = 0;
		setBit(&flags, FLG_MAINLOOP);
	}
}

volatile int debounceMS = 0;
ISR(TIMER4_COMPA_vect)
{
	if (getBit(flags, FLG_DEBOUNCE) && ++debounceMS >= DEBOUNCE_LENGTH)
	{
		clearBit(&flags, FLG_DEBOUNCE);
		debounceMS = 0;
		TIMSK4 &= ~0x02;
	}
}
