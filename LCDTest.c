#include<avr/io.h>
#include"utils/lcd.h"
#include"utils/misc.h"
#include"utils/timer.h"
#include"utils/interrupts.h"

int main(int argc, char** argv)
{
	DDRL = 0xFF;
	PORTL |= 0x10;
	initLCD();
	initTimer();
	LCDClear();
	LCDWriteString("Hello!");
	LCDGotoXY(0,1);
	
	int i = 0;
	while (true)
	{
		LCDGotoXY(0,1);
		LCDWriteInt(i++, -1);
		mTimer(1000);
	}
	return 0;
}
